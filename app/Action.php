<?php

namespace App;

use App\Http\Requests\CreateLogActionRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Action extends Model
{
    use SoftDeletes;
    //
    protected $fillable = [
        'list_id',
        'step',
        'sequence',
        'action_type',
        'action_data',
        'action_name'
    ];

    protected $body;
    protected $subject;

    public static function nextAction($data)
    {
        return self::where('list_id', $data['list_id'])
        ->where('step', $data['step'])
        ->where('sequence','>', $data['sequence'])
		->orderBy('step', 'asc')
		->orderBy('sequence', 'asc')
		->first();

    }

    public static function curAction($data)
    {
        return self::where('list_id', $data['list_id'])
            ->where('step', $data['step'])
            ->where('sequence', $data['sequence'])->first();
    }

    public static function nextActionExist($data)
    {
        return self::where('list_id', $data['list_id'])
            ->where('step', $data['step'])
            ->where('sequence', $data['sequence'])->exist();
    }

    private function explodeEmailData()
    {
        list($body, $subject) = explode('|Subject|', $this->action_data);
        $this->body = $body;
        $this->subject = isset($subject) ? $subject : 'Not subject send';
    }

    /**
     * Get the action's email body.
     *
     * @return string
     */
    public function getEmailBodyAttribute()
    {
        if(!isset($this->body))
        {
            $this->explodeEmailData();
        }


        return $this->body;
    }

    /**
     * Get the action's email subject.
     *
     * @return string
     */
    public function getEmailSubjectAttribute()
    {
        if(!isset($this->subject))
        {
            $this->explodeEmailData();
        }


        return $this->subject;
    }
}
