<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppLog extends Model
{
    //

    protected $fillable = [
        'lead_id',
        'response_text',
        'response_status',
        'phone_number',
        'customer_id',
        'step',
        'sequence',
        'list_id',
        'user',
        'status',
        'email',
    ];
}
