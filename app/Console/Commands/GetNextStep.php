<?php

namespace App\Console\Commands;

use App\Drips\DripGetNextStep;
use Illuminate\Console\Command;

class GetNextStep extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'getnextstep';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $drip;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DripGetNextStep $drip)
    {
        parent::__construct();

        $this->drip = $drip;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->drip->getNextStep();

    }
}
