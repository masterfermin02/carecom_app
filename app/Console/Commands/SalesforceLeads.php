<?php

namespace App\Console\Commands;

use App\Drips\DripGetSalesforceLead;
use Illuminate\Console\Command;

class SalesforceLeads extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gets:sflead';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'INSERT LEADS FROM SALESFORCE';

    protected $drip;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DripGetSalesforceLead $drip)
    {
        parent::__construct();
        $this->drip = $drip;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->drip->GetLeads();
    }
}
