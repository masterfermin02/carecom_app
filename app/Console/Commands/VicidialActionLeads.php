<?php

namespace App\Console\Commands;

use App\Drips\DripLeadAction;
use Illuminate\Console\Command;

class VicidialActionLeads extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:leads_action';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'take the leads from vicidial and make the corresponding action';

    protected $drip;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DripLeadAction $dripLeadAction)
    {
        parent::__construct();

        $this->drip = $dripLeadAction;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->drip->execute();
    }
}
