<?php
/**
 * Created by PhpStorm.
 * User: Developer
 * Date: 10/13/2018
 * Time: 10:39 AM
 */

namespace App\Drips;
use App\Repositories\LeadRepository;
use App\Services\SaleForceApi;
use App\Services\ApiRequest;
class DripGetNextStep {

    protected $saleForceApi;
    protected $db;

    public function __construct(LeadRepository $db, SaleForceApi $service)
    {
        $service->prepare_access_token();
        $this->saleForceApi = $service;
        $this->db = $db;
    }

    public function getNextStep(){
        $this->saveLeads('102');
    }

    public function saveLeads($list_id)
    {
        $data = $this->saleForceApi->post(new ApiRequest(env('SALEFORCE_GETNEXTSTEP_URL'), '{
                "campaignListId" : "'.$list_id.'"
            }
        '));

        if($data == "No Matter Record found" || $data === 'This request don\'t has response.') {
            echo $data;
            return $data;
        }

        $data = collect($data)->map(function($lead){
            return [
                'vendor_lead_code'  => optional($lead)->matterId,
                'list_id'           => optional($lead)->campaignList,
                'first_name'        => optional($lead)->primaryAccountName,
                'phone_number'      => optional($lead)->homePhone,
                'alt_phone'         => optional($lead)->cellPhone,
                'email'             => optional($lead)->email,
                'province'          => optional($lead)->otherEmail,
                'city'              => optional($lead)->billingCity,
                'state'             => optional($lead)->billingState,
                'postal_code'       => optional($lead)->billingPostalCode,
                'address1'          => optional($lead)->billingStreet,
                'address2'          => optional($lead)->matterSSDPreQ,
                'address3'          => optional($lead)->matterSSDApp,
                'middle_initial'    => optional($lead)->Step,
                'security_phrase'   => '1',
                'country_code'      => '0',
                'source_id'         => '0',
                'api_source'        => 'getnextstep'
            ];
        });

        $row_affected = $this->db->create($data->toArray(), 0);
        print_r("rows affected : $row_affected");
        return $data;
    }


}
