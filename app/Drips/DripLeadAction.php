<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17/10/2018
 * Time: 2:23 PM
 */

namespace App\Drips;


use App\Repositories\ViciLeadRepository;
use App\Traits\ActionEnable;

class DripLeadAction
{
    use ActionEnable;

    protected $db;

    public function __construct(ViciLeadRepository $db)
    {
        $this->db = $db;

    }

    public function execute()
    {
        $leads = $this->db->gets();
        collect($leads)->each(function ($lead){
            $this->doCurAction($lead);
        });



    }
}