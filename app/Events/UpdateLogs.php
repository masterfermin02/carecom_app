<?php

namespace App\Events;

use App\AppLog;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UpdateLogs
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $log;
    public $sfdata;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(AppLog $log, $sfdata = [])
    {
        //
        $this->log = $log;
        $this->sfdata = $sfdata;

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
