<?php

namespace App\Http\Controllers;

use App\Action;
use App\Http\Requests\CreateActionRequest;
use App\Http\Requests\UpdateActionRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ActionController extends Controller
{
    //
    public function index()
    {
        return response()->json(['data' => Action::all()], 206);
    }

    public function store(CreateActionRequest $request)
    {
        $data = collect(array_filter($request->actions));
        $first = $data->first();
        $columns = array_keys($first);
        $chunks = $data->chunk(300);
        $rowAffected = 0;
        foreach ($chunks->toArray() as $leads)
        {
            $values = array_map(function($lead){
                $rows = array_values($lead);
                return '('.implode(',',array_map(function($row){ return DB::connection()->getPdo()->quote($row); }, $rows)).')';

            }, $leads);
            $rowAffected += DB::affectingStatement('insert into actions ('.implode(',',$columns).') values'.implode(',', $values));
        }

        return response()->json(['success' => true, 'affected_rows' => $rowAffected], 201);
    }

    public function update(UpdateActionRequest $request, $id)
    {
        //
        $action = Action::find($id);
        $action->fill($request->all());
        $action->save();

        return response()->json(['data' => $action->toArray()], 200);
    }

    public function destroy($list_id)
    {
        DB::table('actions')->where('list_id', $list_id)->update(array('activeflag' => '0'));
        Action::where('list_id', '=', $list_id)->delete();
        return response()->json(['msj' => 'record delete successfuly'], 200);
    }

    public function audio(Request $request)
    {
        if($action = Action::curAction($request->all()))
            return explode('.', $action->action_data)[0];
        return '';
    }
}
