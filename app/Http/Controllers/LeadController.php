<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLeadsRequest;
use App\Lead;
use App\Repositories\LeadRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class LeadController extends Controller
{
    protected $db;

    public function __construct(LeadRepository $repository)
    {
        $this->db = $repository;
    }

    public function index()
    {
        return Lead::all();
    }

    public function store(Request $request)
    {

        $rowAffected = $this->db->create($request->leads, $request->input('replace', 1));
        $data = ['success' => true, 'affected_rows' => $rowAffected];
        Log::channel('leads')->info(json_encode(['leads' => $request->leads, 'response' => $data]));
        return response()->json($data, 201);
    }
}

