<?php

namespace App\Http\Controllers;

use App\Action;
use App\Events\UpdateLogs;
use App\Events\UpdateViciLead;
use App\Http\Requests\MandrillSendRequest;
use App\AppLog;
use App\Traits\ActionEnable;
use Illuminate\Http\Request;
use Mandrill;
use App\Services\SaleForceApi;
use App\Services\ApiRequest;

class MandrillController extends Controller
{
    use ActionEnable;

    private $default_from   = null;

    protected $mandrill;
    protected $saleForceApi;

    //load
    public function __construct(Mandrill  $mandrill, SaleForceApi $saleForceApi){
        $this->mandrill     = $mandrill;
        $this->saleForceApi = $saleForceApi;
    }

    //send email
    public function Send(MandrillSendRequest $request){

        //VALID A VALID EMAIL [FROM]
        $this->default_from = env('MANDRILL_DEFAULT_FROM');
        $log = AppLog::find($request->input('last_insert'));
        $action = Action::curAction([
            'list_id' => $log->list_id,
            'step' => $log->step,
            'sequence' => $log->sequence
        ]);

        $email = $request->input('email');

        //BUILD A EMAIL TO SEND
        $message = array(
            'subject'    => $action->email_subject,
            'html'       => $action->email_body,
            'from_email' => $this->default_from,
            'from_name'  => $this->default_from,
            'to' => array(
                array(
                    'email' =>  $email,
                    'name'  =>  $email
                )
            )
        );
        $data = $request->all();
        $data['phone_number'] = $request->input('phone_number', '');
        $result  = [];
        $disposition = 'Not Sent';
        $log->response_status = 0;
        if(!empty($email))
        {
            try{

                $result = $this->mandrill->messages->send($message);

                $log->response_text = json_encode($result);

                if($result[0]['status'] == 'sent')
                {
                    $log->response_status = 1;
                    $disposition = 'Sent';
                }


            }catch (\Mandrill_Error $e){

                $log->response_text = 'Invalid Data '.get_class($e).'-'.$e->getMessage();
                $log->response_status =  0;
            }

        }


        $this->doNextAction([
            'list_id' => $log->list_id,
            'step' => $log->step,
            'sequence' => $log->sequence,
            'lead_id' => $log->lead_id
        ]);


        $saleForceLogData = [
            'matter_id' => $log->customer_id,
            'update_state' => isset($this->dataToUpdate['country_code']),
            'actions' => [
                [
                    'completed_time' => $request->input('completed_time', date('Y-m-d H:i:s')),
                    'campaign_list_id' => $log->list_id,
                    'step_seq' =>  $log->step.'_'.$log->sequence,
                    'action' => $action->action_name,
                    'disposition' => $disposition,
                    'comments' => $request->input('comments', ''),
                    'content_link' => $request->input('content_link', '')
                ]

            ],
        ];

        event(new UpdateViciLead($this->dataToUpdate));

        // A Mailed event is created and will trigger any relevant
        // observers, such as log
        // code that needs to be run as soon as the email is sent.
        event(new UpdateLogs($log, $saleForceLogData));

        return response()->json([$result]);
    }

    //info
    public function Info(){
        $result = $this->mandrill->users->info();
        return response()->json([$result]);
    }

    //ping
    public function Ping(){
        $result = $this->mandrill->users->ping();
        return response()->json([$result]);
    }

    //senders
    public function Senders(){
        $result = $this->mandrill->users->senders();
        return response()->json([$result]);
    }

    //info email
    public function InfoEmail(Request $request){
        $id = $request->get('id');
        $result = $this->mandrill->messages->info($id);
        return response()->json([$result]);
    }

}
