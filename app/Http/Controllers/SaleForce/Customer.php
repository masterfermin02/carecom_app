<?php

namespace App\Http\Controllers\SaleForce;

use App\Services\ApiRequest;
use App\Services\SaleForceApi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Customer extends Controller
{

    protected $saleForceApi;

    public function __construct(SaleForceApi $service)
    {
        $this->saleForceApi = $service;
    }

    //

    public function index()
    {

        return response()->json(['data' => $this->saleForceApi->post(new ApiRequest('https://kirkendalldwyer--Stage.cs47.my.salesforce.com/services/apexrest/GetCustomers', '{
"StartTime" : "2018-09-03 07:34:32",
"EndTime" :"2018-09-18 07:34:32"
}
'))], 201);
    }
}
