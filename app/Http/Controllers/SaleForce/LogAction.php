<?php

namespace App\Http\Controllers\SaleForce;

use App\Action;
use App\Events\UpdateLogs;
use App\AppLog;
use App\Events\UpdateViciLead;
use App\Repositories\Specifications\AltDialableLeadSpecification;
use App\Traits\ActionEnable;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateLogActionRequest;
use App\Repositories\Vicidial\ViciStatusRepository;
use App\Repositories\Specifications\CompleteVicidialStatusByStatusSpecification;

class LogAction extends Controller
{
    use ActionEnable;

    protected $vicidialRepository;

    public function __construct(ViciStatusRepository $viciStatus)
    {
        $this->vicidialRepository = $viciStatus;
    }

    //

    public function store(CreateLogActionRequest $request)
    {

        $action = Action::curAction([
            'list_id' => $request->list_id,
            'step' => $request->step,
            'sequence' => $request->sequence
        ]);

        $log = new AppLog($request->all());
        $actions = array_map(function($selfAction) use($action) {
            $selfAction['action'] = $action->action_name;
            return $selfAction;
        }, $request->input('actions',[]));

        $saleForceLogData = [
            'matter_id' => $request->input('customer_id', ''),
            'update_state' => false,
            'actions' =>  $actions,
        ];

        if(in_array(optional($request)->dialed_lable, ['MAIN', 'NONE']) && $this->canDialedAlt($request->lead_id))
        {
            // A Mailed event is created and will trigger any relevant
            // observers, such as log
            // code that needs to be run as soon as the email is sent.
            event(new UpdateLogs($log, $saleForceLogData));

            return response()->json($log->sale_force_response, 201);
        }




        if(strtolower($action->action_type) == 'no action')
        {
            $this->dataToUpdate = [
                'source' => 'SF',
                'function' => 'update_lead',
                'search_location' => 'LIST',
                'search_method' => 'LEAD_ID',
                'insert_if_not_found' => 'N',
                'list_id' => $request->list_id,
                'lead_id' => $request->lead_id,
                'country_code' => 1,
                'source_id' => 0,
            ];

        }else{

            $this->doNextAction([
                'list_id' => $request->list_id,
                'step' => $request->step,
                'sequence' => $request->sequence,
                'lead_id' => $request->lead_id
            ]);

            if(strtolower($action->action_type) == 'call' && $this->isCompleteStatus($request->status))
            {
                $this->dataToUpdate['country_code'] = 1;
            }
        }

        $saleForceLogData['update_state'] = isset($this->dataToUpdate['country_code']);

        event(new UpdateViciLead($this->dataToUpdate));


        // A Mailed event is created and will trigger any relevant
        // observers, such as log
        // code that needs to be run as soon as the email is sent.
        event(new UpdateLogs($log, $saleForceLogData));

        return response()->json($log->sale_force_response, 201);

    }

    protected function isCompleteStatus($status)
    {
        return $this->vicidialRepository->exists(new CompleteVicidialStatusByStatusSpecification($status));
    }

    protected function canDialedAlt($lead_id)
    {
        return $this->vicidialRepository->exists(new AltDialableLeadSpecification($lead_id));
    }

}
