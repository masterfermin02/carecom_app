<?php

namespace App\Http\Controllers;

use App\Action;
use App\Events\UpdateLogs;
use App\Events\UpdateViciLead;
use App\Http\Requests\SendSmsRequest;
use App\AppLog;
use App\Traits\ActionEnable;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;
use App\Services\SaleForceApi;
use App\Services\ApiRequest;


class TwilioController extends Controller
{
    use ActionEnable;

    protected $twilio;
    protected $saleForceApi;

    public function __construct(Client $twilio, SaleForceApi $saleForceApi)
    {
        $this->twilio = $twilio;
        $this->saleForceApi = $saleForceApi;
    }
    //

    public function send(SendSmsRequest $request)
    {

        $log = AppLog::find($request->input('last_insert'));
        $action = Action::curAction([
            'list_id' => $log->list_id,
            'step' => $log->step,
            'sequence' => $log->sequence
        ]);

// A Twilio number you own with SMS capabilities
        $twilio_number = $request->input('calledid', "+1".env("TWILIO_DEFAULT_FROM"));
        $message = $action->action_data;
        $jsonResponse = [
            'send' => '1',
            'success' => '1',
            'message' => $message
        ];
        $disposition = 'Sent';
        try {
            $response = $this->twilio->messages->create(
            // Where to send a text message (your cell phone?)
                $request->input('dialprefix', '+1') . $log->phone_number,
                array(
                    'from' => $twilio_number,
                    'body' => $message
                )
            );
            $jsonResponse['response'] = $response->__toString();
            $log->response_text = json_encode($jsonResponse);
            $log->response_status = 1;

        }catch (TwilioException $e) {
            $log->response_text = 'Could not send SMS notification.' .
                ' Twilio replied with: ' . $e;
            $jsonResponse['send'] = 0;
            $jsonResponse['success'] = 0;
            $log->response_status = 0;
            $disposition = 'Not Sent';
        }

        $this->doNextAction([
            'list_id' => $log->list_id,
            'step' => $log->step,
            'sequence' => $log->sequence,
            'lead_id' => $log->lead_id
        ]);

        $saleForceLogData = [
            'matter_id' => $log->customer_id,
            'update_state' => isset($this->dataToUpdate['country_code']),
            'actions' => [
                [
                    'completed_time' => $request->input('completed_time', date('Y-m-d H:i:s')),
                    'campaign_list_id' => $log->list_id,
                    'step_seq' =>  $log->step.'_'.$log->sequence,
                    'action' => $action->action_name,
                    'disposition' => $disposition,
                    'comments' => $request->input('comments', ''),
                    'content_link' => $request->input('content_link', '')
                ]

            ],
        ];

        event(new UpdateViciLead($this->dataToUpdate));

        // A Mailed event is created and will trigger any relevant
        // observers, such as log
        // code that needs to be run as soon as the email is sent.
        event(new UpdateLogs($log, $saleForceLogData));



        return response()->json($jsonResponse, 201);
    }
}
