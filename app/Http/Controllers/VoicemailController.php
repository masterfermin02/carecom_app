<?php

namespace App\Http\Controllers;

use App\Action;
use Illuminate\Http\Request;

class VoicemailController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $res = Action::select('action_data')
                ->where('list_id', '=', $request->get('list_id'))
                ->where('step', '=', $request->get('step'))
                ->where('sequence', '=', $request->get('seq'))
            ->first()->action_data;

       return $res;
    }


}
