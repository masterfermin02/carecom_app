<?php

namespace App\Listeners;

use App\Events\UpdateLogs;
use App\Services\ApiRequest;
use App\Services\SaleForceApi;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogListener
{
    protected $saleForceApi;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(SaleForceApi $saleForceApi)
    {
        //
        $this->saleForceApi = $saleForceApi;
    }

    /**
     * Handle the event.
     *
     * @param  UpdateLogs  $event
     * @return void
     */
    public function handle(UpdateLogs $event)
    {
        //
        $jsonData = json_encode($event->sfdata);
        $event->log->sale_force_response = json_encode($this->saleForceApi->post(new ApiRequest('https://kirkendalldwyer.my.salesforce.com/services/apexrest/LogActions', $jsonData))).'|data|'.$jsonData;
        $event->log->save();
    }
}
