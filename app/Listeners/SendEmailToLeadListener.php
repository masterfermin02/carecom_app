<?php

namespace App\Listeners;

use App\Events\SendEmailToLead;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mandrill;

class SendEmailToLeadListener
{
    protected $mandrill;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mandrill $mandrill)
    {
        //
        $this->mandrill = $mandrill;
    }

    /**
     * Handle the event.
     *
     * @param  SendEmailToLead  $event
     * @return void
     */
    public function handle(SendEmailToLead $event)
    {
        //
        try{

            $result = $this->mandrill->messages->send($event->data);
            $event->response_text = $result;
            $event->response_status = true;
        }catch (\Mandrill_Error $e){

            $result = json_encode([
                'response_text' => 'Invalid Data '.get_class($e).'-'.$e->getMessage(),
                'response_status' => 0
            ]);
            $event->response_text = $result;
            $event->response_status = false;
        }

    }
}
