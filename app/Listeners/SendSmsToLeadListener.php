<?php

namespace App\Listeners;

use App\Events\SendSmsToLead;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;

class SendSmsToLeadListener
{

    protected $twilio;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Client $twilio)
    {
        //
        $this->twilio = $twilio;
    }

    /**
     * Handle the event.
     *
     * @param  SendSmsToLead  $event
     * @return void
     */
    public function handle(SendSmsToLead $event)
    {
        //
        $dialprefix = isset($event->lead['phone_code']) ? '+'.$event->lead['phone_code'] : '+1';
        try {
            $response = $this->twilio->messages->create(
            // Where to send a text message (your cell phone?)
                $dialprefix.$event->lead['phone_number'],
                array(
                    'from' => $event->lead['twilio_number'],
                    'body' => $event->lead['action_data']
                )
            );
            $event->response_text = $response->__toString();


            if(isset($event->lead['alt_phone']) && $event->lead['alt_phone'] != $event->lead['phone_number'])
            {
                $nresponse = $this->twilio->messages->create(
                // Where to send a text message (your cell phone?)
                    $dialprefix.$event->lead['alt_phone'],
                    array(
                        'from' => $event->lead['twilio_number'],
                        'body' => $event->lead['action_data']
                    )
                );
                $event->response_text .= $nresponse->__toString();
            }

            $event->response_status = 1;


        }catch (TwilioException $e) {
            $event->response_text = 'Could not send SMS notification.' .
                ' Twilio replied with: ' . $e;
            $event->response_status = 0;
        }
    }

}
