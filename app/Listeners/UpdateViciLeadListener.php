<?php

namespace App\Listeners;

use App\Events\UpdateViciLead;
use App\Services\ApiRequest;
use App\Services\NoAgentApi;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateViciLeadListener
{

    protected $noAgentApi;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(NoAgentApi $noAgentApi)
    {
        //
        $this->noAgentApi = $noAgentApi;
    }

    /**
     * Handle the event.
     *
     * @param  UpdateViciLead  $event
     * @return void
     */
    public function handle(UpdateViciLead $event)
    {
        //
        $this->noAgentApi->get(new ApiRequest('http://'.env('VICIDIAL_HOST').'/vicidial/itc_non_agent_api.php', $event->data));

    }
}
