<?php

namespace App\Providers;

use App\Services\NoAgentApi;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //

        $this->app->singleton(NoAgentApi::class, function ($app) {

            return new NoAgentApi(env('VICIDIAL_USER'), env('VICIDIAL_PASSWORD'), new Client());
        });
    }
}
