<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class
        ],
        'App\Events\UpdateLogs' => [
            'App\Listeners\LogListener'
        ],
        'App\Events\UpdateViciLead' => [
            'App\Listeners\UpdateViciLeadListener'
        ],
        'App\Events\SendEmailToLead' => [
            'App\Listeners\SendEmailToLeadListener'
        ],
        'App\Events\SendSmsToLead' => [
            'App\Listeners\SendSmsToLeadListener'
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
