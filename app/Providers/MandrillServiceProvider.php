<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Mandrill;

class MandrillServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton(Mandrill::class, function ($app) {
            return new Mandrill(env('MANDRILL_SECRET_KEY'));
        });
    }
}
