<?php

namespace App\Providers;

use App\Repositories\LeadRepository;
use App\Repositories\ViciLeadRepository;
use App\Repositories\Vicidial\ViciStatusRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Lead;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton(LeadRepository::class, function ($app) {
            return new LeadRepository(new Lead());
        });

        $this->app->singleton(ViciLeadRepository::class, function ($app) {
            return new ViciLeadRepository(DB::connection(env('VICI_DB_CONNECTION')));
        });

        $this->app->singleton(ViciStatusRepository::class, function ($app) {
            return new ViciStatusRepository(DB::connection(env('VICI_DB_CONNECTION')));
        });

    }
}
