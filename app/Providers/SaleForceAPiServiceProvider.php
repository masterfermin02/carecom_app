<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\SaleForceApi;

class SaleForceAPiServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton(SaleForceApi::class, function ($app) {

            $service = new SaleForceApi(config('saleforce.grant_type'),
                config('saleforce.client_id'),
                config('saleforce.client_secret'),
                config('saleforce.username'),
                config('saleforce.password'),
                config('saleforce.url'));

            $service->prepare_access_token();
            return $service;
        });
    }
}
