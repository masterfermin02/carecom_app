<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 13/10/2018
 * Time: 11:31 AM
 */

namespace App\Repositories;


use App\Lead;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class LeadRepository implements Repository
{
    protected  $lead;

    public function __construct(Lead $lead)
    {
        $this->lead = $lead;
    }

    public function create($leads, $replace = 0)
    {
        // TODO: Implement create() method.

        $data = collect(array_filter($leads));
        $columns = [];
        $chunks = $data->chunk(300);
        $rowAffected = 0;

        foreach ($chunks->toArray() as $leads)
        {

            if ($replace == 1){
                $values = array_map(function($lead) use(&$columns){
                    $lead['security_phrase'] = '1';
                    $lead['country_code'] = '0';
                    $lead['source_id'] = '0';
                    $lead['middle_initial'] = '1';
                    $lead['api_source'] = 'SF';
                    $lead['created_at'] = now();
                    $lead['updated_at'] = now();
                    if(empty($columns)) $columns = array_keys($lead);
                    $rows = array_values($lead);
                    return '('.implode(',',array_map(function($row){ return DB::connection()->getPdo()->quote($row); }, $rows)).')';

                }, $leads);
            } else {
                $values = array_map(function($lead) use(&$columns){
                    $lead['created_at'] = now();
                    $lead['updated_at'] = now();
                    if(empty($columns)) $columns = array_keys($lead);
                    $rows = array_values($lead);
                    return '('.implode(',',array_map(function($row){ return DB::connection()->getPdo()->quote($row); }, $rows)).')';

                }, $leads);
            }
            $rowAffected += DB::affectingStatement('insert into leads ('.implode(',',$columns).') values'.implode(',', $values));
        }

        return $rowAffected;

    }

}
