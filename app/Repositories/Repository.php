<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 13/10/2018
 * Time: 11:30 AM
 */

namespace App\Repositories;


interface Repository
{
    public function create($data);
}