<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 25/10/2018
 * Time: 2:08 PM
 */

namespace App\Repositories\Specifications;

use App\Repositories\SqlSpecification;


class AltDialableLeadSpecification implements SqlSpecification
{
    protected $lead_id;

    public function __construct($lead_id)
    {
        $this->lead_id = $lead_id;
    }

    public function toSqlQuery(): String
    {
        // TODO: Implement toSqlQuery() method.
        return "
            select l.lead_id from vicidial_list l
            left join vicidial_lists ls on l.list_id = ls.list_id
            left join vicidial_campaigns c on ls.campaign_id = c.campaign_id
            where l.lead_id = {$this->lead_id}
            and c.auto_alt_dial = 'ALT_ONLY'
            and c.auto_alt_dial_statuses like concat('%',l.status,'%')
            and l.alt_phone is not null
            and l.alt_phone <> ''
        ";
    }
}