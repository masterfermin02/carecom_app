<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 25/10/2018
 * Time: 2:08 PM
 */

namespace App\Repositories\Specifications;

use App\Repositories\SqlSpecification;


class CompleteVicidialStatusByStatusSpecification implements SqlSpecification
{
    protected $status;

    public function __construct($status)
    {
        $this->status = $status;
    }

    public function toSqlQuery(): String
    {
        // TODO: Implement toSqlQuery() method.
        return "
            select status from vicidial_statuses where completed = 'Y' and status = '{$this->status}'
        ";
    }
}