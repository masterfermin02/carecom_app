<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 25/10/2018
 * Time: 12:15 PM
 */

namespace App\Repositories\Specifications;


use App\Repositories\SqlSpecification;

class CompleteVicidialStatusSpecification implements SqlSpecification
{


    public function toSqlQuery(): String
    {
        // TODO: Implement toSqlQuery() method.
        return "
            select status, status_name from vicidial_statuses where completed = 'Y'
        ";
    }

}