<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 25/10/2018
 * Time: 11:41 AM
 */

namespace App\Repositories;


interface SqlSpecification extends Specification
{
    public function  toSqlQuery(): String;

}