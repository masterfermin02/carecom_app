<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17/10/2018
 * Time: 11:22 AM
 */

namespace App\Repositories;


use \Illuminate\Database\ConnectionInterface;

class ViciLeadRepository
{
    protected $db;

    public function __construct(ConnectionInterface $db)
    {
        $this->db = $db;
    }


    public function gets()
    {
        return $this->db->select('SELECT lead_id, entry_date, modify_date, status, user, vendor_lead_code as customer_id, source_id, a.list_id, gmt_offset_now, called_since_last_reset, phone_code, phone_number, title, first_name, middle_initial as step, last_name, address1, address2, address3, city, state, province, postal_code, country_code, gender, date_of_birth, alt_phone, email, security_phrase as sequence, comments, called_count, last_local_call_time, rank, owner, 
entry_list_id,
b.campaign_cid
FROM vicidial_list a
left join vicidial_lists ls on a.list_id = ls.list_id
left join vicidial_campaigns b on ls.campaign_id = b.campaign_id
where 
country_code <> 1
and source_id <> 1
and b.active = \'Y\'
and ls.active = \'Y\'
and lead_id = 85
limit 1;');
    }

    public function setTocall($criteria)
    {

        return $this->db->update(" update vicidial_list set source_id=1 where lead_id=?",[$criteria['lead_id']]);
    }

}