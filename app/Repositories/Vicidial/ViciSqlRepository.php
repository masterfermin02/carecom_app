<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 25/10/2018
 * Time: 11:45 AM
 */

namespace App\Repositories\Vicidial;

use App\Repositories\Specification;

interface ViciSqlRepository
{
    public function query(Specification $query);
}