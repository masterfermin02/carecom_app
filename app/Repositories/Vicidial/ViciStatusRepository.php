<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 25/10/2018
 * Time: 11:47 AM
 */

namespace App\Repositories\Vicidial;

use App\Repositories\SqlSpecification;
use \Illuminate\Database\ConnectionInterface;
use App\Repositories\Specification;


class ViciStatusRepository implements ViciSqlRepository
{
    protected $db;

    public function __construct(ConnectionInterface $db)
    {
        $this->db = $db;
    }

    public function query(Specification $specification)
    {
        $specification = self::cast($specification);
        // TODO: Implement query() method.
        return $this->db->select($specification->toSqlQuery());
    }

    public function exists(Specification $specification)
    {
       return count($this->query($specification)) > 0;
    }

    static protected function cast(SqlSpecification $specification): SqlSpecification
    {
        return $specification;

    }

}