<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class UniqueWith implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        // Now that we have our data we can check for the data

        // We first grab the correct table which is passed to the function
        // Now we need to do some checking using Eloquent
        // If you don't understand this, please let me know
       /* $result = DB::table($parameters[0])->where(function($query) use ($attribute, $value, $parameters) {
            $query->where($attribute, '=', $value) // where firstname = value
            ->orWhere($parameters[1]), '=', $value); // where lastname = value
        })->first();*/

        // Now we check if we have a record
        // If we do have a record we return false and the validation will fail
        // If we can't find a record we return true and the validation will succeed
        //return $result ? false : true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must be uniqueWith.';
    }
}
