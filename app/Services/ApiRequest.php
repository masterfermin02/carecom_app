<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 3/10/2018
 * Time: 5:01 PM
 */

namespace App\Services;


class ApiRequest implements ApiServiceInterface
{

    protected $url;
    protected $data;

    public function __construct($url, $data)
    {
        $this->url = $url;
        $this->data = $data;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getData()
    {
        // TODO: Implement getData() method.
        return $this->data;
    }

}