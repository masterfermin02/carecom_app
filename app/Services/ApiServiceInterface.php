<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 3/10/2018
 * Time: 4:59 PM
 */

namespace App\Services;


interface ApiServiceInterface
{

    public function getUrl();

    public function getData();
}