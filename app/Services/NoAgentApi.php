<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 3/10/2018
 * Time: 3:11 PM
 */

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;

class NoAgentApi
{

    protected $username;
    protected $password;
    protected $client;


    public function __construct($username, $password, Client $client)
    {
        $this->username = $username;
        $this->password = $password;

        $this->client = $client;
    }

    Public function get(ApiServiceInterface $api)
    {
        try
        {
            $url = $api->getUrl();
            $option = array('exceptions' => false);

            $data = collect($api->getData())
                ->put('user', $this->username)
                ->put('pass', $this->password)
                ->map(function($item, $key){
                    return $key.'='.$item;
                })
                ->toArray();
            $response = $this->client->get($url.'?'.implode('&', $data),[
                'option' => $option
            ]);
            return $response->getBody()->getContents();
        }
        catch (RequestException $e)
        {
            return $e;
        }
    }

}
