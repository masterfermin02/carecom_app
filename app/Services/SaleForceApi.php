<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 3/10/2018
 * Time: 3:11 PM
 */

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;

class SaleForceApi
{
    protected $API_URL;
    protected $grant_type;
    protected $client_id;
    protected $client_secret;
    protected $username;
    protected $password;
    protected $client;
    protected $accessToken;

    public function __construct($grant_type, $client_id, $client_secret, $username, $password, $url)
    {
        $this->grant_type = $grant_type;
        $this->client_id = $client_id;
        $this->client_secret = $client_secret;
        $this->username = $username;
        $this->password = $password;
        $this->API_URL = $url;

        $this->client = new Client();
    }

    public function prepare_access_token()
    {
        try {
            $url = $this->API_URL;
            $data = [
                'grant_type' => $this->grant_type,
                'client_id' => $this->client_id,
                'client_secret' => $this->client_secret,
                'username' => $this->username,
                'password' => $this->password,
            ];
            $response = $this->client->post($url, [ 'query' => $data ]);
            $result = json_decode($response->getBody()->getContents());
            $this->accessToken = $result->access_token;
        } catch (RequestException $e) {
            $response = $this->StatusCodeHandling($e);
            return $response;
        }

        return $this;
    }

    public function StatusCodeHandling($e)
    {
        if(!$e->hasResponse())
        {
            return 'This request don\'t has response.';

        }
        if ($e->getResponse()->getStatusCode() == '400')
        {
           return json_decode($e->getResponse()->getBody(true)->getContents());
        }
        elseif ($e->getResponse()->getStatusCode() == '422')
        {
            $response = json_decode($e->getResponse()->getBody(true)->getContents());
            return $response;
        }
        elseif ($e->getResponse()->getStatusCode() == '500')
        {
            $response = json_decode($e->getResponse()->getBody(true)->getContents());
            return $response;
        }
        elseif ($e->getResponse()->getStatusCode() == '401')
        {
            $response = json_decode($e->getResponse()->getBody(true)->getContents());
            return $response;
        }
        elseif ($e->getResponse()->getStatusCode() == '403')
        {
            $response = json_decode($e->getResponse()->getBody(true)->getContents());
            return $response;
        }

        $response = json_decode($e->getResponse()->getBody(true)->getContents());
        return $response;
    }

    Public function post(ApiServiceInterface $api)
    {
        if(!$this->accessToken)
        {
            return 'There is not access token to process the request';
        }

        try
        {
            $url = $api->getUrl();
            $option = array('exceptions' => false);
            $header = array('Authorization'=>'Bearer '. $this->accessToken,
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        );
            $data = $api->getData();
            $response = $this->client->post($url,['headers' => $header,
                'body' => $data,
                'option' => $option
            ]);
            $result = $response->getBody()->getContents();
            $data = json_decode($result);
            return (json_last_error() == JSON_ERROR_NONE) ? $data : $result;
        }
        catch (RequestException $e)
        {
            return $this->StatusCodeHandling($e);
        }
    }

}
