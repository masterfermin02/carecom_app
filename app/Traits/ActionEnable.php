<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17/10/2018
 * Time: 2:40 PM
 */

namespace App\Traits;


use App\Action;
use App\Events\SendEmailToLead;
use App\Events\SendSmsToLead;
use App\Events\UpdateLogs;
use App\Events\UpdateViciLead;
use App\Services\ApiRequest;
use App\Services\NoAgentApi;
use App\AppLog;

trait ActionEnable
{
    use StrHelp;

    protected $update_state;
    protected $dataToUpdate;

    public function doCurAction($data)
    {
        if($action = Action::curAction([
            'list_id' => $data->list_id,
            'step' => $data->step,
            'sequence' => $data->sequence
        ]))
        {
            $data->action_data = $action->action_data;
            return call_user_func_array( array($this, strtolower($action->action_type)), [[
                'action_data' => $data->action_data,
                'lead_id' => $data->lead_id,
                'list_id' => $data->list_id,
                'step' => $data->step,
                'sequence' => $data->sequence,
                'email' => $data->email,
                'customer_id' => $data->customer_id,
                'status' => $data->status,
                'comments' => $data->comments,
                'address2' => $data->address2,
                'address3' => $data->address3,
                'address1' => $data->address1,
                'phone_number' => $data->phone_number,
                'user' => $data->user,
                'alt_phone' => $data->alt_phone,
                'campaign_cid' => $data->campaign_cid,
            ]]);

        }
        return false;
    }

    public function doNextAction($data)
    {
        if($action = Action::nextAction($data))
        {

            $this->dataToUpdate = [
                'source' => 'SF',
                'function' => 'update_lead',
                'search_location' => 'LIST',
                'search_method' => 'LEAD_ID',
                'insert_if_not_found' => 'N',
                'list_id' => $action->list_id,
                'lead_id' => $data['lead_id'],
                'security_phrase' => $action->sequence,
                'source_id' => 0,
                'remove_from_hopper' => 'Y',
                'last_action_date_time' => 'NOW'
            ];
            $this->update_state = false;

        }else{

            $this->update_state = true;
            $this->dataToUpdate = [
                'source' => 'SF',
                'function' => 'update_lead',
                'search_location' => 'LIST',
                'search_method' => 'LEAD_ID',
                'insert_if_not_found' => 'N',
                'list_id' => $data['list_id'],
                'lead_id' => $data['lead_id'],
                'country_code' => 1,
                'source_id' => 0,
                'remove_from_hopper' => 'Y',
                'last_action_date_time' => 'NOW'
            ];
        }
    }

    public function call($data)
    {
        if($data['phone_number'] == '9297777777')
        {
            $this->invalidPhoneNumber($data);

        }else{
            event(new UpdateViciLead([
                'source' => 'SF',
                'function' => 'update_lead',
                'search_location' => 'LIST',
                'search_method' => 'LEAD_ID',
                'insert_if_not_found' => 'N',
                'list_id' => $data['list_id'],
                'lead_id' => $data['lead_id'],
                'source_id' => 1,
            ]));
        }



    }

    protected function invalidPhoneNumber($data)
    {
        $this->doNextAction([
            'list_id' => $data['list_id'],
            'lead_id' => $data['lead_id'],
            'sequence' => $data['sequence'],
            'step' => $data['step'],
        ]);
        $applog = AppLog::create([
            'lead_id' => $data['lead_id'],
            'response_text' => 'Invalid phone number',
            'response_status' => 0,
            'phone_number' => $data['phone_number'],
            'customer_id' => $data['customer_id'],
            'sequence' => $data['sequence'],
            'step' => $data['step'],
            'list_id' => $data['list_id'],
            'user' => $data['user'],
            'status' => $data['status'],
            'email' => $data['email']
        ]);
        $saleForceLogData = [
            'matter_id' => $data['customer_id'],
            'update_state' => $this->update_state,
            'actions' => [
                [
                    'completed_time' => date('Y-m-d H:i:s'),
                    'campaign_list_id' => $data['list_id'],
                    'step_seq' => $data['step'].'_'.$data['sequence'],
                    'action' => 'Call',
                    'disposition' => $data['status'],
                    'comments' => $data['comments'],
                    'content_link' => $data['address2'].' '.$data['address3']
                ]

            ],
        ];
        event(new UpdateLogs($applog, $saleForceLogData));
    }

    public function email($data)
    {
        //VALID A VALID EMAIL [FROM]
        $default_from = env('MANDRILL_DEFAULT_FROM');
        //BUILD A EMAIL TO SEND
        $message = array(
            'subject'    => isset($data['subject']) ? $data['subject'] : env('MANDRILL_SUBJECT'),
            'html'       => str_replace('xxx-xxx-xxxx', $this->phoneFormat($data['campaign_cid']), $data['action_data']),
            'from_email' => $default_from,
            'from_name'  => $default_from,
            'to' => array(
                array(
                    'email' =>  $data['email'],
                    'name'  =>  $data['email']
                )
            )
        );
        $event = new SendEmailToLead($message);
        event($event);


        $this->doNextAction([
            'list_id' => $data['list_id'],
            'lead_id' => $data['lead_id'],
            'sequence' => $data['sequence'],
            'step' => $data['step'],
        ]);

        $saleForceLogData = [
            'matter_id' => $data['customer_id'],
            'update_state' => $this->update_state,
            'actions' => [
                [
                    'completed_time' => date('Y-m-d H:i:s'),
                    'campaign_list_id' => $data['list_id'],
                    'step_seq' => $data['step'].'_'.$data['sequence'],
                    'action' => 'Email',
                    'disposition' => $data['status'],
                    'comments' => $data['comments'],
                    'content_link' => $data['address2'].' '.$data['address3']
                ]

            ],
        ];
        // A Mailed event is created and will trigger any relevant
        // observers, such as log
        // code that needs to be run as soon as the email is sent.
        $applog = AppLog::create([
            'lead_id' => $data['lead_id'],
            'response_text' => json_encode($event->response_text),
            'response_status' => $event->response_status,
            'phone_number' => $data['phone_number'],
            'customer_id' => $data['customer_id'],
            'sequence' => $data['sequence'],
            'step' => $data['step'],
            'list_id' => $data['list_id'],
            'user' => $data['user'],
            'status' => $data['status'],
            'email' => $data['email']
        ]);
        event(new UpdateLogs($applog, $saleForceLogData));


    }

    public function  sms($data)
    {
        if($data['phone_number'] == '9297777777')
        {
            $this->invalidPhoneNumber($data);
            return false;
        }

// A Twilio number you own with SMS capabilities
       $data['twilio_number']  = isset($data['callid']) ? '+1'.$data['callid'] :  "+18507907163";
        $data['action_data'] = str_replace('xxx-xxx-xxxx', $this->phoneFormat($data['campaign_cid']), $data['action_data']);
        $event = new SendSmsToLead($data);
        event($event);
        $this->doNextAction([
            'list_id' => $data['list_id'],
            'lead_id' => $data['lead_id'],
            'sequence' => $data['sequence'],
            'step' => $data['step'],
        ]);

        $applog = AppLog::create([
            'lead_id' => $data['lead_id'],
            'response_text' => json_encode($event->response_text),
            'response_status' => $event->response_status,
            'phone_number' => $data['phone_number'],
            'customer_id' => $data['customer_id'],
            'sequence' => $data['sequence'],
            'step' => $data['step'],
            'list_id' => $data['list_id'],
            'user' => $data['user'],
            'status' => $data['status'],
            'email' => $data['email']
        ]);
        $saleForceLogData = [
            'matter_id' => $data['customer_id'],
            'update_state' => $this->update_state,
            'actions' => [
                [
                    'completed_time' => date('Y-m-d H:i:s'),
                    'campaign_list_id' => $data['list_id'],
                    'step_seq' => $data['step'].'_'.$data['sequence'],
                    'action' => 'SMS',
                    'disposition' => $data['status'],
                    'comments' => $data['comments'],
                    'content_link' => $data['address2'].' '.$data['address3']
                ]

            ],
        ];
        event(new UpdateLogs($applog, $saleForceLogData));
        return true;
    }

}