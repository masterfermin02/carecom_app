<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 22/10/2018
 * Time: 4:52 PM
 */

namespace App\Traits;


trait StrHelp
{

    public function phoneFormat($phone_number, $formatt = '/(\d{3})(\d{3})(\d{4})$/')
    {
        if(  preg_match($formatt, $phone_number,  $matches ) )
        {
            return $matches[1] . '-' .$matches[2] . '-' . $matches[3];
        }
        return false;
    }

}