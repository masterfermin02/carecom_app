<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TwilioSmsLog extends Model
{
    //

    protected $fillable = [
        'lead_id',
        'response_text',
        'response_status',
        'phone_number',
        'customer_id',
        'sequence_id',
        'sequence_value',
        'list_id',
        'user',
        'status',
        'email',
    ];
}
