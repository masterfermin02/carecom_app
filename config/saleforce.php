<?php

return [
    'grant_type' => env('SALEFORCE_GRANT_TYPE'),
    'client_id' => env('SALEFORCE_CLIENT_ID'),
    'client_secret' => env('SALEFORCE_CLIENT_SECRET'),
    'username' => env('SALEFORCE_USERNAME'),
    'password' => env('SALEFORCE_PASSWORD'),
    'url'  => env('SALEFORCE_AUTH_URL'),
];