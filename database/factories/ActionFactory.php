<?php

use Faker\Generator as Faker;

$factory->define(\App\Action::class, function (Faker $faker) {
    return [
        'list_id' => $faker->numberBetween(100,1000),
        'step' => $faker->numberBetween(1,5),
        'sequence' => $faker->numberBetween(1,5),
        'action_type' => $faker->text(10),
        'action_data' => $faker->text(100),
        'action_name' => $faker->text(10)
    ];
});
