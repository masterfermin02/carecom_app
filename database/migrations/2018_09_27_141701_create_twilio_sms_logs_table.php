<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTwilioSmsLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('twilio_sms_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lead_id');
            $table->text('response_text');
            $table->boolean('response_status')->default(1);
            $table->string('phone_number');
            $table->string('customer_id')->nullable();
            $table->string('sequence_id')->nullable();
            $table->string('list_id')->nullable();
            $table->string('user')->nullable();
            $table->string('status')->nullable();
            $table->string('email')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('twilio_sms_logs');
    }
}
