<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddResposeSaleForceLogToTwilioLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('twilio_sms_logs', function (Blueprint $table) {
            //
            $table->text('sale_force_response')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('twilio_sms_logs', function (Blueprint $table) {
            //
            $table->dropColumn(['sale_force_response']);
        });
    }
}
