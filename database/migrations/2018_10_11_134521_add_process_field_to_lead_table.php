<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProcessFieldToLeadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leads', function (Blueprint $table) {
            //
            $table->boolean('dialer_processed')->default(0);
            $table->dateTime('dialer_entrydate')->nullable();
            $table->string('dialer_status')->nullable();
            $table->text('dialer_respose')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leads', function (Blueprint $table) {
            //
            $table->dropColumn([
                'dialer_processed',
                'dialer_entrydate',
                'dialer_status',
                'dialer_respose'
            ]);
        });
    }
}
