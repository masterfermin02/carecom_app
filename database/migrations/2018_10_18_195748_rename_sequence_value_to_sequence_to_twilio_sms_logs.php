<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameSequenceValueToSequenceToTwilioSmsLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('twilio_sms_logs', function (Blueprint $table) {
            //
            $table->renameColumn('sequence_value', 'sequence');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('twilio_sms_logs', function (Blueprint $table) {
            //
            $table->renameColumn('sequence', 'sequence_value');
        });

    }
}
