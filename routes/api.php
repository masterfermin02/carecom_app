<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/user', 'UserController@store');
Route::get('/test', 'SaleForce\customer@index');
Route::get('/audio', 'ActionController@audio');
Route::post('/log', 'SaleForce\LogAction@store');

//RETURN VOICEMAIL CONTROLLER
Route::get('/voicemail', 'VoicemailController@show', ['only'=> ['show']]);


Route::group(['middleware' => 'auth:api'],function (){

    Route::group(['prefix' => 'mail'], function(){
        Route::post('/mandrill/send', 'MandrillController@Send');

    });

    Route::group(['prefix' => 'sms'],function(){
        Route::post('/twilio/send', 'TwilioController@send');
    });

    Route::post('/leads', 'LeadController@store');

    Route::resource('/actions', 'ActionController', ['except' => [
        'create', 'edit'
    ]]);

});
