<?php

namespace Tests\Feature;

use App\Action;
use Tests\TestCase;
use Laravel\Passport\Passport;
use App\User;

class ActionTest extends TestCase
{
    public function testRequiredActions()
    {
        Passport::actingAs(
            factory(User::class)->create()
        );

        $this->json('POST', '/api/actions', [])
            ->assertStatus(422)
            ->assertJson([
                'errors' =>[
                    'actions' => ['The actions field is required.']
                ]
            ]);

    }

    public function testActionssMustBeAnArray()
    {
        Passport::actingAs(
            factory(User::class)->create()
        );

        $playLoad = [
            'actions' => 'hola'
        ];
        $this->json('POST', '/api/actions', $playLoad)
            ->assertStatus(422)
            ->assertJson([
                'errors' =>[
                    'actions' => ['The actions must be an array.'],
                ]
            ]);
    }

    public function testActionsMustHasListId()
    {
        Passport::actingAs(
            factory(User::class)->create()
        );

        $playLoad = [
            'actions' => [
                ['action_type' => 123,]
            ]
        ];
        $this->json('POST', '/api/actions', $playLoad)
            ->assertStatus(422)
            ->assertJson(array_reduce(array_keys($playLoad['actions']),function($rules,$key){
                $rules['errors']['actions.'.$key.'.sequence'] = ['The actions.'.$key.'.sequence field is required.'];
                $rules['errors']['actions.'.$key.'.action_name'] = ['The actions.'.$key.'.action_name field is required.'];
                $rules['errors']['actions.'.$key.'.list_id'] = ['The actions.'.$key.'.list_id field is required.'];
                return $rules;
            }));
    }

    public function testCreateAction()
    {
        Passport::actingAs(
            factory(User::class)->create()
        );
        $playload['actions'][] = [
            'list_id' => 123,
            'step' => 1,
            'sequence' => 2,
            'action_type' => 'sms',
            'action_name' => 'sms',
            'action_data' => 'hello world'
        ];
        $this->json('POST', '/api/actions', $playload)
            ->assertStatus(201)
            ->assertJson([ 'success' => true, 'affected_rows' => 1 ]);
    }

    public function testUpdateAction()
    {
        Passport::actingAs(
            factory(User::class)->create()
        );
       $action = Action::create([
            'list_id' => 123,
            'step' => 1,
            'sequence' => 2,
            'action_type' => 'sms',
            'action_data' => 'hello world'
        ]);

        $playload = [
            'list_id' => 124,
            'step' => 2,
            'sequence' => 1,
            'action_type' => 'smsupdate',
            'action_name' => 'sms',
            'action_data' => 'hello world updated'
        ];

        $this->json('PUT', '/api/actions/'.$action->id, $playload)
            ->assertStatus(200)
            ->assertJson([ 'data' => $playload ]);
    }

    public function testActionListedCorrectly()
    {
        Passport::actingAs(
            factory(User::class)->create()
        );

        $actions = factory(Action::class,20)->create();

        $this->json('GET', '/api/actions')
            ->assertStatus(206)
            ->assertJson([
                'data' => $actions->toArray()
            ])
            ->assertJsonStructure([
                'data' => [
                    '*' => ['id', 'list_id', 'step', 'sequence', 'action_type', 'action_data', 'action_name', 'created_at', 'updated_at'],
                ]

            ]);

    }

    public function testDeleteAction()
    {
        Passport::actingAs(
            factory(User::class)->create()
        );
        $actions = factory(Action::class,20)->create();;
        $list_id = $actions->first()->list_id;
        $this->json('DELETE', '/api/actions/'.$list_id)
            ->assertStatus(200);
        $this->assertEquals(Action::where('list_id','=',$list_id)->first(), null);
        $this->assertTrue(Action::count() > 0);
    }

    public function testHtmlEntries()
    {
        $data = json_decode('{
"actions": [{
"step": 1,
"sequence": 1,
"list_id": "00021",
"action_type": "Email",
"action_name": "SSD First Contact Email",
"action_data": "Greetings and thank you for contacting Kirkendall Dwyer LLP.<br><br>We are very interested in further investigating your potential SSD claim. As part of that investigation, we would like to speak with you as soon as possible. If you have not already spoken with one of our intake specialists, please contact us at xxx-xxx-xxxx or <u>click here</u> to schedule a time for us to call you. <br><br>Sincerely,<br>Kirkendall Dwyer<br><br><u>unsubscribe</u>"
}, {
"step": 1,
"sequence": 2,
"list_id": "00021",
"action_type": "SMS",
"action_name": "SSD First Contact SMS",
"action_data": "hello world"
}, {
"step": 1,
"sequence": 3,
"list_id": "00021",
"action_type": "Call",
"action_name": "SSD First Contact Call/VM",
"action_data": "1001_Cartoon-newfmono8Kr.wav"
}]
}');
        $actions = $data->actions;
        Passport::actingAs(
            factory(User::class)->create()
        );
        $playload['actions'] = $actions;
        $this->json('POST', '/api/actions', $playload)
            ->assertStatus(201)
            ->assertJson([ 'success' => true, 'affected_rows' => count($playload['actions']) ]);
    }

    public function testNotAudio()
    {


        Passport::actingAs(
            factory(User::class)->create()
        );

        $playload['step'] = 1;
        $playload['sequence'] = 3;
        $playload['list_id'] = 101;
        $this->json('GET', '/api/audio', $playload)
            ->assertStatus(200)
            ->assertSee('');

    }

    public function testAudio()
    {

        $action = factory(Action::class)->create([
            'step' => 1,
            'sequence' => 3,
            'action_type' => 'Call',
            'action_name' => 'SSD First Contact Call/VM',
            'action_data' => '1001_Cartoon-newfmono8Kr.wav',
        ]);
        Passport::actingAs(
            factory(User::class)->create()
        );

        $playload['step'] = $action->step;
        $playload['sequence'] = $action->sequence;
        $playload['list_id'] = $action->list_id;
        $this->json('GET', '/api/audio', $playload)
            ->assertStatus(200)
            ->assertSee('1001_Cartoon-newfmono8Kr');

    }
}
