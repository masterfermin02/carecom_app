<?php

namespace Tests\Feature;

use Tests\TestCase;
use Laravel\Passport\Passport;
use App\User;
use App\Lead;

class LeadTest extends TestCase
{

    public function testcreateLeads()
    {
        Passport::actingAs(
            factory(User::class)->create()
        );

        $playLoad = [
            'leads' => [
                ['list_id' => '123', 'phone_number' => '1234567898'],
                ['list_id' => '124', 'phone_number' => '1234567893'],
            ]
        ];
        $this->json('POST', '/api/leads', $playLoad)
            ->assertStatus(201);

        $leads = Lead::get(['list_id','phone_number'])->toArray();
        $this->assertEquals($leads, $playLoad['leads']);
    }

    public function test_thousand_Leads()
    {
        Passport::actingAs(
            factory(User::class)->create()
        );
        $playLoad = [];
        for($i = 0; $i < 10; $i++)
        {
            $playLoad['leads'][] = ['list_id' => '123', 'phone_number' => '1267'.$i];
        }
        $this->json('POST', '/api/leads', $playLoad)
            ->assertStatus(201);
        $last_phone_number = $playLoad['leads'][9]['phone_number'];
        $this->assertEquals($last_phone_number, Lead::max('phone_number'));

    }

    public function test_address_suppor_special_char()
    {
        Passport::actingAs(
            factory(User::class)->create()
        );
        $playLoad =
        [
            "leads" => [
                ["vendor_lead_code" => 1, "source_id" => 123, "list_id" => 123, "title" => "sr", "first_name" => "juan", "middle_initial" => "dsd", "last_name" => "perez", "address1" => "avenida siempre viva", "address2" => "", "address3" => "", "city" => "sprinfig", "state" => "sf", "province" => "test", "country_code" => "813", "gender" => "M", "date_of_birth" => "1991-08-08", "alt_phone" => 1, "email" => "test@test.com", "security_phrase" => 123456, "comments" => "test comment", "entry_list_id" => 145, "phone_number" => "1234567878"],
                ["vendor_lead_code" => 1, "source_id" => 123, "list_id" => 123, "title" => "sr", "first_name" => "juan", "middle_initial" => "dsd", "last_name" => "perez", "address1" => "avenida siempre ' \" #  http:\\goole.com viva", "address2" => "", "address3" => "", "city" => "sprinfig", "state" => "sf", "province" => "test", "country_code" => "813", "gender" => "M", "date_of_birth" => "1991-08-08", "alt_phone" => 1, "email" => "test@test.com", "security_phrase" => 123456, "comments" => "test comment", "entry_list_id" => 145, "phone_number" => "1234567878"]
            ]
        ];

        $this->json('POST', '/api/leads', $playLoad)
            ->assertStatus(201)
            ->assertJson([ 'success' => true, 'affected_rows' => count($playLoad['leads']) ]);;

    }
}
