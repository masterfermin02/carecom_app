<?php

namespace Tests\Feature;

use App\Action;
use App\Events\UpdateLogs;
use App\Events\UpdateViciLead;
use Tests\TestCase;
use Laravel\Passport\Passport;
use App\User;
use Illuminate\Support\Facades\Event;

class LogActionTest extends TestCase
{

    public function testRequiredLeadIdPhoneNumber()
    {
        Passport::actingAs(
            factory(User::class)->create()
        );

        $this->json('POST', '/api/log')
            ->assertStatus(422)
            ->assertJson([
                'errors' =>[
                    'phone_number' => ['The phone number field is required.'],
                    'lead_id' => ['The lead id field is required.'],
                    'list_id' => ['The list id field is required.'],
                    'step' => ['The step field is required.'],
                    'sequence' => ['The sequence field is required.'],
                ]
            ]);

    }

    public function testSaveLog()
    {
        Passport::actingAs(
            factory(User::class)->create()
        );
        $this->createActions();
        $playload = [
            'customer_id' => 'a081C000012cxVi',
            'lead_id' => 1,
            'list_id' => 00021,
            'step' => 1,
            'sequence' => 1,
            'phone_number' => 1237887474,
            'actions' => [
                [
                    'completed_time' => date('Y-m-d H:i:s'),
                    'campaign_list_id' => 00021,
                    'step_seq' => 1,
                    'action' => 'call',
                    'disposition' => 'NA',
                    'comments' => 'test',
                    'content_link' => 'http://google.com'
                ]

            ],
            'response_text' => 'call',
            'response_status' => 1,
            'user' => 'test',
            'status' => 'test',
            'email' => 'test@test.com'
        ];
        Event::fake();
        $this->json('POST', '/api/log', $playload)
            ->assertStatus(201);

        Event::assertDispatched(UpdateLogs::class, function ($e) use ($playload) {
            return $e->log->response_text == $playload['response_text'] && $e->sfdata['actions'] == $playload['actions'];
        });

        Event::assertDispatched(UpdateViciLead::class, function ($e){
            return $e->data['source_id'] == 0;
        });
    }

    public function testNextAciton()
    {

        $this->createActions();
        $action = Action::nextAction([
            'list_id' => 00021,
            'step' => 1,
            'sequence' => 1
        ]);
        $this->assertTrue(isset($action), 'the action is set');

        $action = Action::nextAction([
            'list_id' => 00021,
            'step' => 1,
            'sequence' => 2
        ]);

        $this->assertTrue(!isset($action), 'the action should be null');

    }

    private function createActions()
    {
        factory(Action::class)->create([
            'list_id' => 00021,
            'step' => 1,
            'sequence' => 1,
            'action_name' => 'call'
        ]);

        factory(Action::class)->create([
            'list_id' => 00021,
            'step' => 1,
            'sequence' => 2,
            'action_name' => 'sms'
        ]);

    }
}
