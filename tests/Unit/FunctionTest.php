<?php

namespace Tests\Unit;

use App\Events\SendEmailToLead;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FunctionTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testTestCallUserFuncArray()
    {
        $this->assertTrue(call_user_func_array( array($this, strtolower('returnTrue')), []));
    }

    public function returntrue()
    {
        return true;
    }

    public function testSendEmail()
    {
        //VALID A VALID EMAIL [FROM]
        $default_from = env('MANDRILL_DEFAULT_FROM');
        $data['email'] = 'masterfermin02@hotmail.com';
        //BUILD A EMAIL TO SEND
        $message = array(
            'subject'    => isset($data['subject']) ? $data['subject'] : env('MANDRILL_SUBJECT'),
            'text'       => 'testing',
            'from_email' => $default_from,
            'from_name'  => $default_from,
            'to' => array(
                array(
                    'email' =>  $data['email'],
                    'name'  =>  $data['email']
                )
            )
        );
        Event::fake();

        $event = new SendEmailToLead($message);
        event($event);

        Event::assertDispatched(SendEmailToLead::class, function ($e) use ($message) {
            return $e->data['from_email'] === $message['from_email'];
        });
    }
}
