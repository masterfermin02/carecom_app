<?php

namespace Tests\Unit;


use App\Action;
use Tests\TestCase;

class SendEmailToLeadTest extends TestCase
{

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testSendEmailTolead()
    {
        $this->assertTrue(true);


    }

    public function testNumberFormat()
    {
        $data = " Greetings and thank you for contacting Kirkendall Dwyer LLP.<br><br>We are very interested in further investigating your potential claim. As part of that investigation, we would like to speak with you as soon as possible.  If you have not already spoken with one of our intake specialists, please contact us at xxx-xxx-xxxx or <u>click here</u> to schedule a time for us to call you.   <br><br>Sincerely,<br>Kirkendall Dwyer<br><br><u>unsubscribe</u>";
        $experted = " Greetings and thank you for contacting Kirkendall Dwyer LLP.<br><br>We are very interested in further investigating your potential claim. As part of that investigation, we would like to speak with you as soon as possible.  If you have not already spoken with one of our intake specialists, please contact us at 123-456-7878 or <u>click here</u> to schedule a time for us to call you.   <br><br>Sincerely,<br>Kirkendall Dwyer<br><br><u>unsubscribe</u>";
        $phone_number = '1234567878';
        $result = '';

        if(  preg_match( '/(\d{3})(\d{3})(\d{4})$/', $phone_number,  $matches ) )
        {
            $result = $matches[1] . '-' .$matches[2] . '-' . $matches[3];
        }

        $new_data = str_replace('xxx-xxx-xxxx', $result, $data);

        $this->assertEquals( '123-456-7878',$result);
        $this->assertEquals( $experted,$new_data);
    }

    public function testSubjectSplit()
    {

        $expertedBody = html_entity_decode ("Greetings and thank you for contacting Kirkendall Dwyer LLP.<br><br>We are very interested in further investigating your potential SSD claim. As part of that investigation, we need to speak with you as soon as possible. <br><br>If you have not already spoken with one of our intake specialists, please contact us at 1-877-214-4407.<br><br>We look forward to speaking with you.<br><br>Sincerely, <br>SSD Intake Team<br>Kirkendall Dwyer LLP");
        $expertedSubject = "Social Security Disability";

        $action = new Action(['action_data' => $expertedBody.'|Subject|'.$expertedSubject]);

        $this->assertEquals( $expertedBody, $action->email_body);
        $this->assertEquals( $expertedSubject, $action->email_subject);
    }

    /*public function testDripLeadAction()
    {
        $drip = new DripLeadAction(new ViciRepositoryMock());

        $drip->execute();

        $this->assertTrue(true);


    }*/
}
